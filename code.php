<?php

class Person {

	public $firstName;
	public $middleName;
	public $lastName;


	public function __construct($firstName,$middleName,$lastName){
	
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}

	public function printName(){
		return "Your full name is $this->firstName $this->lastName";
	}
}


$person = new Person("Senku","", "Ishigami");


Class Developer extends Person{

	public function printName(){
		return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer.";
	}


}

$developer = new Developer("John", "Finch", "Smith");

Class Engineer extends Person{

	public function printName(){
		return "You are an engineer named $this->firstName $this->middleName $this->lastName.";
	}


}

$engineer = new Engineer("Harold", "Myers", "Reese");


/* supplementary activity */

class Character {

	public $name;

	public function __construct($name){
		$this->name = $name;
	}

}

Class VoltesMember extends Character{
     public $vehicle;

     public function __construct($name,$vehicle){
		parent::__construct($name);
		$this->vehicle = $vehicle;
	}

     public function printName(){
		return "Hi, I am $this->name! I pilot the Volt $this->vehicle";
	}

}

$member1 = new VoltesMember("Steve", "Cruiser");
$member2 = new VoltesMember("Big Bert", "Panzer");
$member3 = new VoltesMember("Little John", "Frigate");
$member4 = new VoltesMember("Jamie", "Lander");
$member5 = new VoltesMember("Mark", "Bomber");