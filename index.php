<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
     <head>
          <meta charset="UTF-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Activity s03</title>
     </head>
     <body>
          <h1>Person</h1>
          <p><?= $person->printName() ?></p>
          <br>
          <h1>Developer</h1>
          <p><?= $developer->printName() ?></p>
          <br>
          <h1>Engineer</h1>
          <p><?= $engineer->printName() ?></p>
          <br>
          <h1>Voltes V</h1>
          <p><?php var_dump($member1) ?></p>
          <p><?php var_dump($member2) ?></p>
          <p><?php var_dump($member3) ?></p>
          <p><?php var_dump($member4) ?></p>
          <p><?php var_dump($member5) ?></p>
          <p><?= $member1->printName() ?></p>
          <p><?= $member2->printName() ?></p>
          <p><?= $member3->printName() ?></p>
          <p><?= $member4->printName() ?></p>
          <p><?= $member5->printName() ?></p>
     </body>
</html>